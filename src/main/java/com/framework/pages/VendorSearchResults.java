package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;

public class VendorSearchResults extends SeleniumBase{
	
	public VendorSearchResults() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using="//tr[2]/td[1]") WebElement eleVendorName;
	public VendorSearchResults printVendorName() {
		String elementText = getElementText(eleVendorName);
		System.out.println(elementText);
		return this;
	}
	
	

}
