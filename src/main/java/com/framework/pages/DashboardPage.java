package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;

public class DashboardPage extends SeleniumBase{
	
	Actions builder = new Actions(driver);
	
	public DashboardPage() {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how = How.XPATH,using="//button[text()=' Vendors']") WebElement eleVendor;
	public DashboardPage mouseOverVendor() {
		builder.moveToElement(eleVendor).perform();
		return this;
	}
	
	@FindBy(how = How.LINK_TEXT,using="Search for Vendor") WebElement eleSearchForVendor;
	public VendorsSearchPage clickSearchForVendors() {
		builder.moveToElement(eleSearchForVendor).click().perform();
		return new VendorsSearchPage();
	}

}
