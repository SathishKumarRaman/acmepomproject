package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;

public class VendorsSearchPage extends SeleniumBase{

	public VendorsSearchPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="vendorTaxID") WebElement eleVendorTaxId;
	public VendorsSearchPage enterVendorTaxId(String data) {
		clearAndType(eleVendorTaxId, data);
		return this;
	}
	
	@FindBy(how = How.ID,using="buttonSearch") WebElement eleSearchButton;
	public VendorSearchResults clickSearch() {
		click(eleSearchButton);
		return new VendorSearchResults();
	}
}
