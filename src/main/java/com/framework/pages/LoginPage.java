package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.framework.api.SeleniumBase;

public class LoginPage extends SeleniumBase{
	
	public LoginPage() {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how = How.ID,using="email") WebElement eleEmail;
	public LoginPage enterEmail(String data) {
		clearAndType(eleEmail, data);
		return this;
	}
	
	@FindBy(how = How.ID,using="password") WebElement elepwd;
	public LoginPage enterPassword(String data) {
		clearAndType(elepwd, data);
		return this;
	}
	
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLoginButton;
	public DashboardPage clickLogin() {
		click(eleLoginButton);
		return new DashboardPage();
	}

}
