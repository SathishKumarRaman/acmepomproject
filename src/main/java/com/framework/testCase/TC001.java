package com.framework.testCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001 extends ProjectMethods{
	
	@BeforeTest
	public void InitializeTestScript() {
		testCaseName = "TC001";
		testDescription = "ACME Vendor Search";
		testNodes = "ACME";
		author = "Sathish";
		category = "smoke";
		dataSheetName = "UseCase2";
	}
	
	@Test(dataProvider="fetchData")
	public void testCase001(String uname, String pwd, String VTaxID) {
		new LoginPage()
		.enterEmail(uname)
		.enterPassword(pwd)
		.clickLogin()
		.mouseOverVendor()
		.clickSearchForVendors()
		.enterVendorTaxId(VTaxID)
		.clickSearch()
		.printVendorName();
		
	}

}
